using System;
using GitNet.DB;
using NHibernate;
using NHibernate.Tool.hbm2ddl;

namespace SchemaGenerator
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			var config = GitNet.DB.Repository.NHibernateHelper.GetConfiguration();
			var gen = new SchemaUpdate(config);
			gen.Execute(true, false);
		}
	}
}
