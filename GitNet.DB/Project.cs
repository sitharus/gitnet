using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GitNet.DB
{
	public class Project
	{
		public virtual int ProjectId { get; set; }
		
		[Required]
		[Display(Name = "Name")]
		public virtual string Name { get; set; }

		public virtual string Owner { get; set; }

		public virtual string UrlName { get; set; }
		
		[Display(Name = "Description")]
		public virtual string Description { get; set; }

		public virtual IList<Repo> Repositories { get; set; }

		public virtual IList<Component> Components { get; set; }

		public virtual IList<Priority> Priorities { get; set; }
		
		public virtual IList<Status> Statuses { get; set; }
		
		public virtual IList<Resolution> Resolutions { get; set; }
		
		public virtual IList<Milestone> Milestones { get; set; }

        public virtual IList<ProjectRole> UserRoles { get; set; }
	}

    public enum ProjectRoles {
        User,
        Administrator,
        Custom
    }
}

