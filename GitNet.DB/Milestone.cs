using System;
using System.ComponentModel.DataAnnotations;

namespace GitNet.DB
{
	public class Milestone
	{
		public virtual int MilestoneId { get; set; }
		
		[Required]
		[Display(Name = "Name")]
		public virtual string Name { get; set; }
		
		[Display(Name = "Due Date")]
		public virtual DateTime? DueDate { get; set; }
		
		public virtual Project Project { get; set; }
	}
}

