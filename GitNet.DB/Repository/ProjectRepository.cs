using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;

namespace GitNet.DB.Repository
{
	public class ProjectRepository
	{
		private ISession _Session;
		
		public ProjectRepository(ISession session)
		{
			_Session = session;	
		}
		
		public IList<Project> GetAll(string username)
		{
			return (from item in _Session.QueryOver<DB.Project>()
				        where item.Owner == username
				        select item).List();
		}
		
		public bool Add(DB.Project project)
		{
			if (string.IsNullOrEmpty(project.UrlName))
			{
				project.UrlName = Utilities.StringUtilities.CreateUrlName(project.Name, project.Owner);
			}
			if (project.Components == null)
			{
				project.Components = new List<Component> {
					new Component {
						Name = "Default",
						Project = project
					}
				};
			}
			if (project.Priorities == null)
			{
				project.Priorities = new List<Priority> {
					new Priority {
						Name = "High",
						Rank = 0,
						Project = project
					},
					new Priority {
						Name = "Medium",
						Rank = 1,
						Project = project
					},
					new Priority {
						Name = "Low",
						Rank = 2,
						Project = project
					}
				};
			}
			
			if (project.Statuses == null)
			{
				project.Statuses = new List<Status> {
					new Status {
						Name = "New",
						Rank = 0,
						Project = project
					},
					new Status {
						Name = "Accepted",
						Rank = 1,
						Project = project
					},
					new Status {
						Name = "Resolved",
						Rank = 2,
						IsResolved = true,
						Project = project
					},
					new Status {
						Name = "Closed",
						Rank = 3,
						IsClosed = true,
						Project = project
					},
				};
			}
			
			if (project.Resolutions == null)
			{
				project.Resolutions = new List<Resolution> {
					new Resolution {
						Name = "Fixed",
						Rank = 0,
						Project = project
					},
					new Resolution {
						Name = "Won't Fix",
						Rank = 1,
						Project = project
					},
					new Resolution {
						Name = "Cancelled",
						Rank = 2,
						Project = project
					}
				};
			}
			
			if (project.Milestones == null)
			{
				project.Milestones = new List<Milestone> {
					new Milestone {
						Name = "First Release",
						Project = project
					}
				};
			}
			using (ITransaction transaction = _Session.BeginTransaction())
			{
				var existing = (from item in _Session.QueryOver<Project>()
				                where item.UrlName == project.UrlName
				                select item).RowCount() > 0;
				if (existing)
				{
					throw new Exceptions.DuplicateEntityException();	
				}
				_Session.Save(project);
				transaction.Commit();
				return true;
			}
		}
		
		public Project Get(string urlName)
		{
			return (from item in _Session.QueryOver<Project>()
						where item.UrlName == urlName
				        select item).Take(1).List()[0];
		}
		
		public void Save()
		{
			using (ITransaction transaction = _Session.BeginTransaction())
			{
				_Session.Flush();
				transaction.Commit();
			}
		}
	}
}

