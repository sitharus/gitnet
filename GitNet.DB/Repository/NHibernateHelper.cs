using NHibernate;
using NHibernate.Cfg;

namespace GitNet.DB.Repository
{
	public class NHibernateHelper
	{
		private static ISessionFactory _sessionFactory;
 
		private static ISessionFactory SessionFactory {
			get {
				if (_sessionFactory == null) {
					_sessionFactory = GetConfiguration ().BuildSessionFactory ();
				}
				return _sessionFactory;
			}
		}
 
		public static ISession OpenSession ()
		{
			return SessionFactory.OpenSession ();
		}

        public static void CloseSession(ISession session)
        {
            using (ITransaction transaction = session.BeginTransaction())
             {
                 session.Flush();
                 transaction.Commit();
             }
            session.Close();
        }

		public static Configuration GetConfiguration ()
		{
			var configuration = new Configuration ();
			configuration.Configure ();
			configuration.AddAssembly (typeof(DB.Repo).Assembly);
			return configuration;
		}
	}
}

