using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;

namespace GitNet.DB.Repository
{
	public class RepositoryRepository
	{
		private ISession _Session;
		
		public RepositoryRepository(ISession session) {
				_Session = session;
		}
		
		public IList<DB.Repo> GetAll(string UserName) {
			return (from item in _Session.QueryOver<DB.Repo>()
				    where item.Owner == UserName
			  	    select item).List();
		}

        public DB.Repo Get(string username, string repositoryName) {
            return (from item in _Session.QueryOver<DB.Repo>()
                    where item.Owner == username && item.Name == repositoryName
                    select item).Take(1).List()[0];
        }
		
		public DB.Repo GetByUrl(string repositoryUrl) {
            if (repositoryUrl.EndsWith(".git"))
            {
                repositoryUrl = repositoryUrl.Substring(0, repositoryUrl.Length - 4);
            }
            return (from item in _Session.QueryOver<DB.Repo>()
                    where item.UrlName == repositoryUrl
                    select item).Take(1).List()[0];
        }
		
		public bool Add(DB.Repo repo) {
			
			if (string.IsNullOrEmpty(repo.UrlName)) {
				repo.UrlName = Utilities.StringUtilities.CreateUrlName(repo.Name, repo.Owner);	
			}
			using (ITransaction transaction = _Session.BeginTransaction())
			{
				var existing = (from item in _Session.QueryOver<DB.Repo>()
				                where item.UrlName == repo.UrlName
				                select item).RowCount() > 0;
				if (existing) {
					throw new Exceptions.DuplicateEntityException();	
				}
				_Session.Save(repo);
				transaction.Commit();
                return true;
			}
		}

        public RepoRole RolesFor(DB.Repo repo, string username)
        {
            var results =  (from item in _Session.QueryOver<DB.RepoRole>()
                            where item.Repo == repo && item.User == username
                            select item).Take(1).List();

            if (results.Count > 0) {
                return results[0];
            }
            return null;
        }

        public bool CheckPermissions(DB.Repo repo, string username, bool write = false, bool admin = false)
        {
            var query = from item in _Session.QueryOver<DB.RepoRole>()
                        where item.Repo == repo && item.User == username
                        select item;
            if (write) {
                query = query.Where(i => i.Write);
            }
            if (admin)
            {
                query = query.Where(i => i.Administer);
            }
            return query.RowCount() > 0;
        }
	}
}


