using System;
using System.ComponentModel.DataAnnotations;

namespace GitNet.DB
{
	public class Status
	{
		public virtual int StatusId { get; set; }
		
		[Required]
		[Display(Name = "Name")]
		public virtual string Name { get; set; }
		
		[Required]
		[Display(Name = "Rank")]
		public virtual int Rank { get; set; }
		
		[Display(Name = "Resolved")]
		public virtual bool IsResolved { get; set; }

		[Display(Name = "Closed")]
		public virtual bool IsClosed { get; set; }

		
		public virtual Project Project { get; set; }
	}
}

