using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GitNet.DB
{
	public class Repo
	{
		public virtual int RepositoryId { get; set; }
		
		
        [Required]
        [StringLength(200, MinimumLength=3)]
        [Display(Name="Name")]
		public virtual string Name { get; set; }
		
		[StringLength(2000)]
        [Display(Name="Description")]
        public virtual string Description { get; set; }

        [Display(Name="Public Repository")]
        [DefaultValue(true)]
        public virtual bool Public { get; set; }

		
        public virtual string UrlName { get; set; }
		public virtual string Owner { get; set; }
		public virtual Project Project { get; set; }
        public virtual IList<RepoRole> UserRoles { get; set; }
	}
}

