using System;
using System.Collections.Generic;

namespace GitNet.DB
{
    public class ProjectRole
    {
        public virtual Project Project { get; set; }
        public virtual string User { get; set; }
        public virtual bool IsAdministrator { get; set; }
        public virtual IList<string> CustomRoles { get; set; }

        public override bool Equals (object obj)
        {
            if (obj is ProjectRole)
            {
                var other = obj as ProjectRole;
                return (Project == other.Project && User == other.User);
            }
            return base.Equals (obj);
        }

        public override int GetHashCode ()
        {
            return (Project.ProjectId.ToString() + User).GetHashCode();
        }
    }
}

