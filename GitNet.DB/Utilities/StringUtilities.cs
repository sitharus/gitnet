using System;
using System.IO;

namespace Utilities
{
	public static class StringUtilities
	{
        public static string CreateUrlName(string name, string owner)
        {
            var sanitisedName = SanitiseName(name);
            var sanitisedOwner = SanitiseName(owner);
            return String.Concat(sanitisedOwner, "/", sanitisedName);
        }

        public static string SanitiseName(string name) {
            var forbidden = Path.GetInvalidFileNameChars();
            foreach (var c in forbidden) {
                name = name.Replace(c, '-');
            }
            return name.Replace(' ', '-');
        }
	}
}

