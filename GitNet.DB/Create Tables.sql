
CREATE TABLE Repository (
	RepositoryId INTEGER PRIMARY KEY,
	Name VARCHAR(200) NOT NULL,
	Owner VARCHAR(200) NOT NULL,
	UrlName VARCHAR(200) NOT NULL,
	Description TEXT
);

CREATE TABLE RepositoryRole (
	RepositoryId INTEGER NOT NULL REFERENCES Repository(RepositoryId),
	UserId VARCHAR(200) NOT NULL,
	RoleCode VARCHAR(200) NOT NULL CHECK (RoleCode IN ("VIEW", "FORK", "PUSH", "MERGE", "ADMIN")),
	PRIMARY KEY(RepositoryId, UserId, RoleCode)
);

CREATE TABLE Project (
	ProjectId INTEGER PRIMARY KEY,
	Owner varchar(200) NOT NULL,
	Name VARCHAR(200) NOT NULL,
	UrlName VARCHAR(200) NOT NULL,
	Description TEXT
);

CREATE TABLE ProjectRole (
	ProjectId INTEGER NOT NULL REFERENCES Project(ProjectId),
	UserId VARCHAR(200) NOT NULL,
	RoleCode VARCHAR(200) NOT NULL CHECK (RoleCode IN ("Member", "Admin", "ManageMilestones", "ManageIssues", "ManageReleases", "CreateIssue", "EditIssue"))
);

CREATE TABLE Milestone (
	MilestoneId INTEGER PRIMARY KEY,
	Owner VARCHAR(200),
	Name VARCHAR(200) NOT NULL,
	TargetDate INTEGER,
	Description TEXT
);
	

CREATE TABLE Issue (
	IssueId INTEGER PRIMARY KEY,
	IssueNumber INTEGER NOT NULL,
	ProjectId INTEGER NOT NULL REFERENCES Project(ProjectId),
	Owner VARCHAR(200),
	Title VARCHAR(400) NOT NULL,
	Description TEXT NOT NULL,
	Status VARCHAR(100) NOT NULL REFERENCES ProjectStatusCode(Status),
	Priority VARCHAR(100) NOT NULL REFERENCES ProjectPriorityCode(Priority),
	MilestoneId INTEGER NOT NULL REFERENCES Milestone(MilestoneId),
	CreatedAt INTEGER NOT NULL,
	UpdatedAt INTEGER NOT NULL
);

CREATE TABLE IssueHistory (
	IssueHistoryId INTEGER PRIMARY KEY,
	IssueId INTEGER NOT NULL REFERENCES Issue(IssueId),
	CreatedAt INTEGER NOT NULL,
	ChangeType VARCHAR(200) NOT NULL CHECK (ChangeType IN ("Note", "Title", "Description", "Status", "Priority", "Milestone")),
	ChangeData TEXT NOT NULL
);