using System;

namespace GitNet.DB
{
    public class RepoRole
    {
        public virtual Repo Repo { get; set; }
        public virtual string User { get; set; }
        public virtual bool Write { get; set; }
        public virtual bool Administer { get; set; }

        public override bool Equals (object obj)
        {
            if (obj is RepoRole)
            {
                var other = obj as RepoRole;
                return (Repo == other.Repo && User == other.User);
            }
            return base.Equals (obj);
        }

        public override int GetHashCode ()
        {
            return (Repo.RepositoryId.ToString() + User).GetHashCode();
        }
    }
}

