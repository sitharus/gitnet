using System;
using System.ComponentModel.DataAnnotations;

namespace GitNet.DB
{
	public class Priority
	{
		public virtual int PriorityId { get; set; }
		
		[Required]
		[Display(Name = "Name")]
		public virtual string Name { get; set; }
		
		[Required]
		[Display(Name = "Rank")]
		public virtual int Rank { get; set; }
		
		public virtual Project Project { get; set; }
	}
}

