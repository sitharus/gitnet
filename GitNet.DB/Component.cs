using System;
using System.ComponentModel.DataAnnotations;

namespace GitNet.DB
{
	public class Component
	{
		public virtual int ComponentId { get; set; }
		
        [Required]
        [Display(Name = "Name")]
		public virtual string Name { get; set; }
		
		[Display(Name = "Archived")]
		public virtual bool Archived {get; set;}
		
        public virtual string Owner { get; set; }
        public virtual Project Project { get; set; }
	}
}

