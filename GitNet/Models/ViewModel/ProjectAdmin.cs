using System;
using GitNet.DB;
using System.Collections.Generic;

namespace GitNet.Models.ViewModel
{
    public class ProjectAdmin
    {
        public Project Project { get; set; }
        public List<ProjectRoleView> UserRoles { get; set; }
    }
}

