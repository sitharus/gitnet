using System;
using System.ComponentModel.DataAnnotations;

namespace GitNet.Models.ViewModel
{
	public class Register
	{
		[Required]
        [StringLength(200, MinimumLength=4)]
		[Display(Name = "Username")]
		public string Username {get; set;}
		
		[Required]
        [StringLength(200, MinimumLength=6)]
		[Display(Name = "Email Address")]
		public string Email {get; set;}
		
		[Required]
        [StringLength(200, MinimumLength=4)]
		[Display(Name = "Password")]
		public string Password {get; set;}
		
	}
}

