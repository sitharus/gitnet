using System;

namespace GitNet.Models.ViewModel
{
    public class ProjectRoleView
    {
        public string UserId { get; set; }
        public bool IsOwner { get; set; }
        public bool IsAdministrator { get; set; }
        public bool Remove { get; set; }
        public string[] CustomRoles { get; set; }
    }
}

