using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace GitNet.Models.ViewModel
{
	public class LogOnModel
	{
		[Required]
        [StringLength(200, MinimumLength=4)]
		[Display(Name="User Name")]
		[ScaffoldColumn(false)]
		public string UserName {get; set;}
		
        [Required]
        [StringLength(200, MinimumLength=4)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
		
	}
}

