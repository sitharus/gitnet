using System;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace GitNet.Authorisation
{
    /// <summary>
    /// This http module converts any response with the magic SuppressAuthenticationKey in
    /// context.items to a 401 forbidden. This is because you can't return a 401 natively
    /// though MVC in any sane way when using forms auth, but git clients can't do cookies.
    ///
    /// See Authorisation/AuthorizeRepository.cs for the other half.
    /// </summary>
    public class HttpBasicPrompter : IHttpModule
    {
        public const string SuppressAuthenticationKey = "GitNet__SuppressRedirect";
        private static bool AllowBasicAuth = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings.Get("AllowBasicAuth"));
        private static string BasicAuthRealm = System.Configuration.ConfigurationManager.AppSettings.Get("GitBasicAuthRealm");

        public String ModuleName {
            get { return "HttpBasicPrompter"; }
        }

        public void Init(HttpApplication context)
        {
            context.EndRequest += OnEndRequest;
        }

        private void OnEndRequest(object source, EventArgs args)
        {
            var context = (HttpApplication)source;
            var response = context.Response;

            if (context.Context.Items.Contains(SuppressAuthenticationKey) && AllowBasicAuth && context.Response.StatusCode == 302) {
                response.TrySkipIisCustomErrors = true;
                response.ClearContent();
                response.StatusCode = 401;
                response.RedirectLocation = null;
                response.AddHeader("WWW-Authenticate", String.Concat("Basic realm=\"", BasicAuthRealm, "\""));
            }
        }

        public void Dispose()
        {
        }
    }
}

