using System;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using GitNet.DB;
using GitNet.Utilities;

namespace GitNet.Authorisation
{
    public class AuthorizeRepository : AuthorizeAttribute
    {
        private static bool AllowBasicAuth = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings.Get("AllowBasicAuth"));
        private static string AdminRole = System.Configuration.ConfigurationManager.AppSettings.Get("AdministratorRole");

        public bool Write { get; set; }

        public bool Admin { get; set; }

        public bool Use401 { get; set; }

        private Tuple<bool, bool, bool> IsValid(AuthorizationContext filterContext)
        {
            var ownername = filterContext.RouteData.Values["username"];
            var reponame = filterContext.RouteData.Values["reponame"];
            var repoRepo = new DB.Repository.RepositoryRepository(ControllerExtensions.DBContextFromHttp(filterContext.HttpContext));
            var repo = repoRepo.GetByUrl(String.Concat(ownername, "/", reponame));

            var request = filterContext.HttpContext.Request;
            var valid = false;
            var read = repo.Public;
            var write = false;
            var admin = false;

            // If basic auth is allowed and this action would otherwise use a 401 status
            // then parse out an HTTP Basic auth header if present.
            if (AllowBasicAuth && Use401 && !String.IsNullOrEmpty(request.Headers["Authorization"])) {
                var cred = System.Text.ASCIIEncoding.ASCII.GetString(
                    Convert.FromBase64String(request.Headers["Authorization"].Substring(6)))
                    .Split(':');
                if (Membership.ValidateUser(cred[0], cred[1])) {
                    var user = new FormsAuthenticationTicket(1, cred[0], DateTime.Now, DateTime.Now.AddHours(1), false, "");
                    var identity = new FormsIdentity(user);
                    HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(identity, new string[0]);
                }
            }

            if (filterContext.HttpContext.User.Identity.IsAuthenticated) {
                var requestingUser = filterContext.HttpContext.User.Identity.Name;
                var permissions = repoRepo.RolesFor(repo, requestingUser);

        
                // Test the permissions applied directly to the repo
                if (filterContext.HttpContext.User.IsInRole(AdminRole)) {
                    read = true;
                    write = true;
                    admin = true;
                }
                if (requestingUser == repo.Owner) {
                    read = true;
                    write = true;
                    admin = true;
                }
                else {
                    if (permissions != null) {
                        read = true;
                        write = permissions.Write;
                        admin = permissions.Administer;
                    }
                }
            }

            valid = (Admin && admin) || (!Admin && Write && (write || admin)) || (!Admin && !Write && read);

            if (Use401) {
                filterContext.HttpContext.Items["GitNet__SuppressRedirect"] = "true";
            }

            return Tuple.Create(valid, write, admin);

        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var results = IsValid(filterContext);
            if (results.Item1) {
                filterContext.RouteData.DataTokens["WriteAccess"] = results.Item2;
                filterContext.RouteData.DataTokens["AdminAccess"] = results.Item3;
    
                HttpCachePolicyBase cachePolicy = filterContext.HttpContext.Response.Cache;
                cachePolicy.SetProxyMaxAge(new TimeSpan(0));
                cachePolicy.AddValidationCallback(CacheValidateHandler, null);
            }
            else {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }

        protected override HttpValidationStatus OnCacheAuthorization(HttpContextBase httpContext)
        {
            if (httpContext == null) {
                throw new ArgumentNullException("httpContext");
            }

            return HttpValidationStatus.IgnoreThisRequest;
        }

        private void CacheValidateHandler(HttpContext context, object data, ref HttpValidationStatus validationStatus)
        {
            validationStatus = OnCacheAuthorization(new HttpContextWrapper(context));
        }

    }
}

