using System;
using System.Linq;
using System.Collections.Generic;
using GitNet.Models.ViewModel;
using GitNet.DB;
using GitNet.GitClient;
using System.Web.Routing;
using System.Web.Mvc;

namespace GitNet.Utilities
{
    public static class GitHtmlHelpers
    {
        public static object UrlForFile(Repo repo, FileInfo file) {
            var path = String.Join("/", file.CommitId, file.FullPath);
            return new {
                username = "",
                reponame = repo.UrlName,
                path = path
            };
        }

        public static RouteValueDictionary FromThisAction(this UrlHelper urlHelper, IDictionary<string, object> newValues)
        {
            var existingRoutes = urlHelper.RequestContext.RouteData.Values;
            var newDict = new Dictionary<string, object>((IDictionary<string, object>)existingRoutes);
            foreach (var kv in newValues) {
                newDict[kv.Key] = kv.Value;
            }

            return new RouteValueDictionary(newDict);
        }
    }
}

