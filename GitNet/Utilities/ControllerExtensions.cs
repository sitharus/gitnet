using System;
using System.Web;
using System.Web.Mvc;
using NHibernate;

namespace GitNet.Utilities
{
    public static class ControllerExtensions
    {
        public static ISession DBContext(this Controller controller) {
            return DBContextFromHttp(controller.HttpContext);
        }

        public static ISession DBContextFromHttp(HttpContextBase context) {
            return (ISession) context.Items["DBSession"];
        }
    }
}

