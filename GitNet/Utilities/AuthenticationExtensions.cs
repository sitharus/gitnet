using System;
using System.Linq;
using System.Web.Security;

namespace GitNet.Utilities
{
	public static class AuthenticationExtensions
	{
		
		public static Boolean IsFormsAuthEnabled() {
			var property = typeof(FormsAuthentication).GetProperties().FirstOrDefault(p => p.Name == "IsEnabled");
			if (property != null) {
				return (Boolean) property.GetValue(typeof(FormsAuthentication), null);
			}
			return true;
		}
	}
}

