using System;
using System.IO;
using System.Collections.Generic;
using AutoMapper;
using GitNet.Models.ViewModel;
using GitNet;


namespace GitNet
{
    public static class AutomapperConfig
    {
        public static void Configure()
        {
            Mapper.AssertConfigurationIsValid();
        }
    }
}

