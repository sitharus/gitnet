﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using NHibernate;

namespace GitNet
{
	public class MvcApplication : System.Web.HttpApplication
	{
		
		public static void RegisterRoutes (RouteCollection routes)
		{
			routes.IgnoreRoute ("{resource}.axd/{*pathInfo}");

			
			routes.MapRoute(
				"GitInfoRefs",
				"Git/{username}/{reponame}.git/info/refs",
				new { controller = "Git", action = "InfoRefs" });
			
			routes.MapRoute(
				"GitInfoUploadPack",
				"Git/{username}/{reponame}.git/git-upload-pack",
				new { controller = "Git", action = "UploadPack" });
			
			routes.MapRoute(
				"GitInfoReceivePack",
				"Git/{username}/{reponame}.git/git-receive-pack",
				new { controller = "Git", action = "ReceivePack" });


             routes.MapRoute(
                 "GitRepository",
                 "Git/{username}/{reponame}.git/",
                 new { controller = "RepositoryBrowser", action = "Show", id="" });
			
			
			routes.MapRoute(
				"GitRepositoryBrowser",
				"Git/{username}/{reponame}/{action}",
				new {controller = "RepositoryBrowser", action="Show", id=""}
				);

            routes.MapRoute(
                "GitRepositoryObjectBrowser",
                "Git/{username}/{reponame}/object/{id}",
                new {controller = "RepositoryBrowser", action="BrowseObject", id=""}
             );

            routes.MapRoute(
                "GitRepositoryFileViewer",
                "Git/{username}/{reponame}/{*path}",
                new {controller = "RepositoryBrowser", action="Browse", id=""}
             );

            routes.MapRoute(
                "Project",
                "Project/{username}/{identifier}/{action}/{id}",
                new {controller = "Project", action = "Show", id = ""}
            );

			
			routes.MapRoute (
                "Default",
                "{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = "" }
            );
			
		}

		protected void Application_Start ()
		{
            AutomapperConfig.Configure();
			RegisterRoutes (RouteTable.Routes);
		}

        protected void Application_BeginRequest()
        {
            Context.Items["DBSession"] = GitNet.DB.Repository.NHibernateHelper.OpenSession();
        }

        protected void Application_EndRequest()
        {
            var session = ((NHibernate.ISession)Context.Items["DBSession"]);

             GitNet.DB.Repository.NHibernateHelper.CloseSession(session);
        }
	}
}
