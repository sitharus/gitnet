
$(document).ready(function() {
	var tabbedRegions = $("div.tabbed");
	tabbedRegions.prepend("<div class='tab-header'></div>")
	tabbedRegions.each(function(idx) {
		var headers = $(this).children("section").children("h1:first-child");
		$(this).find(".tab-header").append(headers);
		var selectedTab = $(this).children("section").index($(this).children("section.current-tab"));
		selectedTab = selectedTab == -1 ? 0 : selectedTab;
		
		$(this).find(".tab-header > h1").eq(selectedTab).addClass("current-tab");
		$(this).find(".tab-header h1").click(function(e) {
			var position = $(this).prevAll().length;
			var sections = $(this).parent().parent().find(">section");
			sections.removeClass("current-tab");
			$(this).siblings().removeClass("current-tab");
			$(this).addClass("current-tab");
			$(sections.get(position)).addClass("current-tab");
		})
	});
})

var GitNet = {};

