$(document).ready(function() {
	$("#add-component").click(GitNet.ProjectAdmin.addComponent)
	$("#add-priority").click(GitNet.ProjectAdmin.addPriority)
	$("#add-status").click(GitNet.ProjectAdmin.addStatus)
	$("#add-resolution").click(GitNet.ProjectAdmin.addResolution)
});


GitNet.ProjectAdmin = {
	inputNameMatch: /\[(\d+)\]/,
	inputIdMatch: /_(\d+)__/,
	addComponent: function(e) {
		e.preventDefault();
		var rowToClone = GitNet.ProjectAdmin.cloneLastRow("#components-table", "tbody tr");
		var nameEditor = $(rowToClone).find("input[name$=Name]")[0];
		nameEditor.setAttribute("type", "text");
		$(nameEditor).next("span").hide();
	},
	addPriority: function(e) {
		e.preventDefault();
		GitNet.ProjectAdmin.cloneLastRow("#priority-list", ">li");
	},
	addStatus: function(e) {
		e.preventDefault();
		GitNet.ProjectAdmin.cloneLastRow("#status-list", ">li");
	},
	addResolution: function(e) {
		e.preventDefault();
		GitNet.ProjectAdmin.cloneLastRow("#resolution-list", ">li");
	},
	cloneLastRow: function(container, item) {
		var parent = $(container);
		var rowToClone = parent.find(item).last().clone();
		$(rowToClone).find("input").each(this.incrementInputNumber);
		parent.append(rowToClone);
		return rowToClone;
	},
	incrementInputNumber: function(i, el) {
		var idNum = parseInt(GitNet.ProjectAdmin.inputNameMatch.exec(el.getAttribute("name"))[1]);
		var nextId = idNum + 1;
		if (el.hasAttribute("id")) 
			el.setAttribute("id", el.getAttribute("id").replace(GitNet.ProjectAdmin.inputIdMatch, "_"+nextId+"__"));
		el.setAttribute("name", el.getAttribute("name").replace(GitNet.ProjectAdmin.inputNameMatch, "["+nextId+"]"));
		if (el.value && !(el.getAttribute("type") == "checkbox" || (el.getAttribute("type") == "hidden" && $(el).prev("input")[0] && $(el).prev("input")[0].getAttribute("type") == "checkbox")))
			el.value = "";
		if (el.checked)
			el.checked = false;
	}
}