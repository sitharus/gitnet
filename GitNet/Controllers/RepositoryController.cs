using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Security;
using GitNet.Models;
using Automation = GitNet.GitClient.RepositoryAutomation;
using AutoMapper;

namespace Controllers
{
	public class RepositoryController : Controller
	{
		
		[Authorize]
		public ActionResult New() {
			return View ();	
		}
		
		[Authorize]
		[HttpPost]
		public ActionResult New(GitNet.DB.Repo repository) {
            var session = GitNet.DB.Repository.NHibernateHelper.OpenSession();
			var repositoryRepository = new GitNet.DB.Repository.RepositoryRepository(session);
			
			var username = Membership.GetUser().UserName;
            repository.Owner = username;
            var modelRepo = Mapper.Map<GitNet.DB.Repo>(repository);

			try {
                repositoryRepository.Add(modelRepo);
                Automation.CreateRepository(modelRepo.UrlName);
			} catch {
				Automation.DeleteRepository(modelRepo.UrlName);
                if (session.Contains(modelRepo)) {
                    session.Delete(modelRepo);
                       session.Flush();
                }
				throw;
			}
            return RedirectToRoute("GitRepositoryBrowser", new {username = "", reponame = modelRepo.UrlName});
		}
	}
}

