using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Security;
using GitNet.DB.Repository;
using GitNet.Utilities;
using AutoMapper;

namespace Controllers
{
	[HandleError]
	public class HomeController : Controller
	{
		public ActionResult Index ()
		{
			ViewBag.Message = "Welcome to ASP.NET MVC on Mono!";
			return View ();
		}
		
		[Authorize]
		public ActionResult About()
		{
			return View ();
		}
		
		[Authorize]
		public ActionResult Dashboard()
		{
			ViewBag.Repositories = new RepositoryRepository(this.DBContext()).GetAll(User.Identity.Name);
            ViewBag.Projects = new ProjectRepository(this.DBContext()).GetAll(User.Identity.Name);
			return View();	
		}
	}
}

