using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using AutoMapper;
using GitNet.Utilities;
using GitNet.GitClient;
using GitNet.Authorisation;

namespace GitNet.Controllers
{
    [AuthorizeRepository]
    public class RepositoryBrowserController : Controller
    {
        /// <summary>
        /// Shows the overview of the repository with a log of recent history
        /// </summary>
        public ActionResult Show(string username, string reponame, int page = 1)
        {
            ViewBag.Page = page;
            var model = GetRepository(username, reponame);

            ViewBag.RepoUrlName = model.UrlName;
            try {
                var gitRepo = GitClient.RepositoryHelper.GetRepository(username, reponame);
                ViewBag.AllRefs = GitClient.RepositoryReader.ListRefs(gitRepo);
                ViewBag.History = GitClient.RepositoryReader.History(gitRepo);
            }
            catch (Exception e) {
                return new HttpNotFoundResult(e.Message);
            }

            ViewBag.Title = String.IsNullOrWhiteSpace(model.Name) ? "title!" : model.Name ;
            return View(model);
        }

        /// <summary>
        /// Displays an object specified by commitid and path parsed from the URL. The object can be
        /// either a subtree or file.
        /// </summary>
        public ActionResult Browse(string username, string reponame, string path)
        {
            var model = GetRepository(username, reponame);
            ViewBag.RepoUrlName = model.UrlName;

            string treePath;
            string commit;
            if (path == null)
            {
                treePath = "";
                commit = "HEAD";
            }
            else
            {
                var pathParts = path.Split(new[]{'/'});
                commit = pathParts[0];
                treePath = String.Join("/", pathParts.Skip(1));
            }

            var gitRepo = GitClient.RepositoryHelper.GetRepository(username, reponame);
            try {
                var isSubtree = GitClient.RepositoryReader.IsSubtree(gitRepo, commit, treePath);
                ViewBag.IsSubtree = isSubtree;

                if (isSubtree) {
                    ViewBag.Files = GitClient.RepositoryReader.ListTree(gitRepo, commit, treePath);
                }
                else {
                    ViewBag.History = GitClient.RepositoryReader.History(gitRepo, treePath);
                    //ViewBag.FileBlob = GitClient.RepositoryReader.GetBlob(gitRepo, commit, treePath);
                }

                ViewBag.Path = path;
            }
            catch (NullReferenceException) {
                return new HttpStatusCodeResult(404);
            }
            return View(model);
        }


        /// <summary>
        /// Displays the object identified by id.
        /// </summary>
        public ActionResult BrowseObject(string username, string reponame, string id)
        {
            var model = GetRepository(username, reponame);
            ViewBag.RepoUrlName = model.UrlName;

            var gitRepo = GitClient.RepositoryHelper.GetRepository(username, reponame);
            switch (GitClient.RepositoryReader.ObjectKind(gitRepo, id)) {
            case ObjectType.Blob:
                break;
            case ObjectType.Tag:
                break;
            case ObjectType.Tree:
                break;
            case ObjectType.Commit:
                var diff = GitClient.RepositoryReader.CommitDiff(gitRepo, id);
                var commit = GitClient.RepositoryReader.History(gitRepo, commitId: id)[0];
                ViewBag.RawDiff = diff;
                ViewBag.Commit = commit;
                return View("CommitDiff", model);
            }
            return new HttpStatusCodeResult(404);
        }

        [AuthorizeRepository(Admin = true)]
        [HttpGet]
        public ActionResult Admin(string username, string reponame)
        {
            var model = GetRepository(username, reponame);
            ViewBag.RepoUrlName = model.UrlName;
            return View(model);
        }

        [AuthorizeRepository(Admin = true)]
        [HttpPost]
        public ActionResult Admin(string username, string reponame, DB.Repo input)
        {
            var model = GetRepository(username, reponame);
            ViewBag.RepoUrlName = model.UrlName;

            model.Description = input.Description;
            model.Public = input.Public;

            return View(model);
        }


        private GitNet.DB.Repo GetRepository(string username, string reponame)
        {
            var repositoryRepository = new DB.Repository.RepositoryRepository(this.DBContext());
            var db = repositoryRepository.GetByUrl(String.Concat(username, "/", reponame));
            return db;
        }

        /// <summary>
        /// Grabs the repository permissions for the current user from the routedata.
        /// </summary>
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            base.OnActionExecuting(ctx);
            ViewBag.CanWrite = RouteData.DataTokens["WriteAccess"];
            ViewBag.CanAdmin = RouteData.DataTokens["AdminAccess"];
        }
    }
}

