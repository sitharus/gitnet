using System;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

using GitNet.Authorisation;

using NGit;
using NGit.Transport;

namespace Controllers
{
    [AuthorizeRepository(Use401 = true)]
	public class GitController : Controller
	{
		public static string RepoRoot = System.Configuration.ConfigurationManager.AppSettings.Get("RepositoryRoot");
		public static string[] Services = new [] {"upload-pack", "receive-pack"};
		public const String UPLOAD_PACK_REQUEST_TYPE = "application/x-git-upload-pack-request";
		public const String UPLOAD_PACK_RESULT_TYPE = "application/x-git-upload-pack-result";
		public const String RECEIVE_PACK_REQUEST_TYPE = "application/x-git-receive-pack-request";
		public const String RECEIVE_PACK_RESULT_TYPE = "application/x-git-receive-pack-result";

        /// <summary>
        /// The info-ref URL is used by Git to determine if the 'smart' API is available.
        /// This implements the minimum required, as all other calls are for the old DAV
        /// API which we don't support.
        /// </summary>
		public ActionResult InfoRefs(String username, String reponame, String service)
		{
            Response.AddHeader("Accept-Encoding", "gzip");
			if (!string.IsNullOrEmpty(service)) {
				var gitService = ValidateService(service);
				var contentType = String.Format("application/x-git-{0}-advertisement", gitService);
				var serviceBeacon = String.Format("# service=git-{0}\n", gitService);
                Response.ContentType = contentType;
				
				
				if (gitService == "upload-pack") {
                    Response.BufferOutput = false;
					var pack = new UploadPack(GetRepository(username, reponame));
					var packetOut = new PacketLineOut(Response.OutputStream);
					packetOut.WriteString(serviceBeacon);
					packetOut.End();
					try
                    {
						pack.SetBiDirectionalPipe(false);
						var adv = new RefAdvertiser.PacketLineOutRefAdvertiser(packetOut);
						pack.SendAdvertisedRefs(adv);
					}
					finally {
						pack.GetRevWalk().Release();
					}

					return new EmptyResult();
				}
				else if (gitService == "receive-pack") {
                    if (!((bool) RouteData.DataTokens["WriteAccess"])) {
                        return new HttpUnauthorizedResult();
                    }

                    Response.BufferOutput = false;
					var pack = new ReceivePack(GetRepository(username, reponame));
					var packetOut = new PacketLineOut(Response.OutputStream);
					packetOut.WriteString(serviceBeacon);
					packetOut.End();
					try
                    {
						pack.SetBiDirectionalPipe(false);
						var adv = new RefAdvertiser.PacketLineOutRefAdvertiser(packetOut);
						pack.SendAdvertisedRefs(adv);
					}
					finally
                    {
						pack.GetRevWalk().Release();
					}
					return new EmptyResult();
				}
				return new HttpStatusCodeResult(403, "Unsupported Service");
			}
			else { // Old dumb protocol.
				return new HttpStatusCodeResult(403, "Unsupported Service");
			}
		}

        /// <summary>
        /// Upload pack uploads the requested pack to the client. These are named from the
        /// server's point of view, so this is the read method.
        /// </summary>
		[HttpPost]
		public ActionResult UploadPack(String username, string reponame)
		{
            Response.AddHeader("Accept-Encoding", "gzip");
			if (Request.ContentType != UPLOAD_PACK_REQUEST_TYPE || !Request.AcceptTypes.Contains(UPLOAD_PACK_RESULT_TYPE)) {
				return new HttpStatusCodeResult(403, "Unsupported Service");
			}
            Response.BufferOutput = false;
            Response.ContentType = UPLOAD_PACK_RESULT_TYPE;
			try {

				var pack = new UploadPack(GetRepository(username, reponame));

				pack.SetBiDirectionalPipe(false);
				pack.Upload(Request.InputStream, Response.OutputStream, null);
                Response.End();
				return new EmptyResult();
			}
			catch (UploadPackMayNotContinueException e) {
				if (e.IsOutput())
                {
					return Content("", UPLOAD_PACK_RESULT_TYPE);
				}
				else
                {
					return new HttpStatusCodeResult(403, e.ToString()); 
				}
			}
			catch (Exception e) {
				// TODO error logging
				Console.Write(e.ToString());
				return new HttpStatusCodeResult(403, e.ToString()); 
			}
		}

        /// <summary>
        /// Receive pack recieves the packs from the client. These are named from the
        /// server's point of view, so this is the write method.
        /// </summary>
        [AuthorizeRepository(Use401 = true, Write = true)]
		[HttpPost]
		public ActionResult ReceivePack(string username, string reponame)
		{
            Response.BufferOutput = false;
            Response.AddHeader("Accept-Encoding", "gzip");
			if (Request.ContentType != RECEIVE_PACK_REQUEST_TYPE || !Request.AcceptTypes.Contains(RECEIVE_PACK_RESULT_TYPE)) {
				return new HttpStatusCodeResult(403, "Unsupported Service");
			}

			try {
                Console.WriteLine("Starting pack receieve");
                Response.ContentType = RECEIVE_PACK_RESULT_TYPE;
                Response.BufferOutput = false;
				var pack = new ReceivePack(GetRepository(username, reponame));
				pack.SetBiDirectionalPipe(false);
				pack.Receive(Request.InputStream, Response.OutputStream, null);
                Response.Close();
                Console.WriteLine("Completed pack receieve");

				return new EmptyResult();
			}
			catch (Exception e) {
				// TODO error logging
				Console.Write(e.ToString());
				return new HttpStatusCodeResult(403, e.ToString()); 
			}
		}


		private Repository GetRepository(string username, string repo)
		{
			var path = GitNet.GitClient.RepositoryHelper.RepoPath(username, repo);
			if (!Directory.Exists(path)) {
				throw new InvalidOperationException("Unsupported repository");
			}
			return new RepositoryBuilder().SetGitDir(new Sharpen.FilePath(path)).ReadEnvironment().FindGitDir().Build();
		}
		
		private string ValidateService(String service)
		{
			if (!service.StartsWith("git-") || !Services.Contains(service.Substring(4))) {
				throw new InvalidDataException(String.Format("Unsupported service: '{0}'", service));
			}
			// TODO : auth?
			return service.Substring(4);
		}
	}
}

