using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Security;
using GitNet.Models.ViewModel;

namespace Controllers
{
	public class AccountController : Controller
	{
		public ActionResult LogOn ()
		{
			return View ();
		}
		
		[HttpPost]
		public ActionResult LogOn (LogOnModel user, string returnUrl)
		{
			if (Membership.ValidateUser(user.UserName, user.Password)) {
				FormsAuthentication.SetAuthCookie (user.UserName, false);
				if (Url.IsLocalUrl (returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith ("/")
	                        && !returnUrl.StartsWith ("//") && !returnUrl.StartsWith ("/\\")) {
					return Redirect (returnUrl);
				} else {
					return RedirectToAction ("Dashboard", "Home");
				}
			} else {
				ModelState.AddModelError("", "The user name or password provided is incorrect.");
				return View (user);
			}
		}
		
		public ActionResult Register(string returnUrl)
		{
			ViewBag.ReturnUrl = returnUrl;
			return View ();	
		}
		
		[HttpPost]
		public ActionResult Register(GitNet.Models.ViewModel.Register regInfo, string returnUrl)
		{
			var user = Membership.CreateUser(regInfo.Username, regInfo.Password, regInfo.Email);
			FormsAuthentication.SetAuthCookie(user.UserName, false);
			
			if (Url.IsLocalUrl (returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith ("/")
                        && !returnUrl.StartsWith ("//") && !returnUrl.StartsWith ("/\\")) {
				return Redirect (returnUrl);
			} else {
				return RedirectToAction ("Index", "Home");
			}
		}
		
		public ActionResult LogOff() {
			FormsAuthentication.SignOut();
			return RedirectToAction ("Index", "Home");
		}
	}
}

