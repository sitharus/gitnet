using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Security;
using GitNet.Models.ViewModel;
using GitNet.DB.Repository;
using GitNet.DB;
using GitNet.Utilities;
using AutoMapper;

namespace GitNet.Controllers
{
    public class ProjectController : Controller
    {
        [Authorize]
        public ActionResult New()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult New(Project project)
        {
            project.Owner = User.Identity.Name;
            new ProjectRepository(this.DBContext()).Add(project);
            return RedirectToRoute("Project", new {username="", identifier = project.UrlName});
        }

        [Authorize]
        public ActionResult Show(string username, string identifier)
        {
            var project = new ProjectRepository(this.DBContext()).Get(String.Concat(username, "/", identifier));
            return View(project);
        }

        [Authorize]
        public ActionResult Admin(string username, string identifier)
        {
            var project = new ProjectRepository(this.DBContext()).Get(String.Concat(username, "/", identifier));

            var UserRoles = (new List<ProjectRoleView> {
                new ProjectRoleView {
                    UserId = project.Owner,
                    IsOwner = true,
                    CustomRoles = new string[0]
                }
            }).Concat(project.UserRoles.Select(ur => new ProjectRoleView {
                UserId = ur.User,
                IsOwner = false,
                IsAdministrator = ur.IsAdministrator,
                CustomRoles = ur.CustomRoles.ToArray()
            })).OrderBy(urv => urv.IsOwner).ThenBy(urv => urv.UserId).ToList();



            return View(new ProjectAdmin{ Project = project, UserRoles = UserRoles });
        }

        [Authorize]
        [HttpPost]
        public ActionResult Components(string username, string identifier, ProjectAdmin model)
        {
            var repo = new ProjectRepository(this.DBContext());
            var project = repo.Get(String.Concat(username, "/", identifier));
            var Components = model.Project.Components;
            Components = Components.Where(c => !String.IsNullOrEmpty(c.Name)).ToList();

            for (int i = 0; i < Components.Count; i++) {
                if (i < project.Components.Count) {
                    project.Components[i].Archived = Components[i].Archived;
                } else  if (!string.IsNullOrEmpty(Components[i].Name)) {
                    Components[i].Project = project;
                    project.Components.Add(Components[i]);
                }
            }
            repo.Save();
            return RedirectToRoute("Project", new {username="", identifier = project.UrlName, action = "Admin"});
        }

        [Authorize]
        [HttpPost]
        public ActionResult Priorities(string username, string identifier, ProjectAdmin model) {
            var priorities = model.Project.Priorities;
            var repo = new ProjectRepository(this.DBContext());
            var project = repo.Get(String.Concat(username, "/", identifier));
            for (int i = 0; i < priorities.Count; i++) {
                if (i < project.Priorities.Count) {
                    project.Priorities[i].Name = priorities[i].Name;
                } else if (!string.IsNullOrEmpty(priorities[i].Name)) {
                    priorities[i].Project = project;
                    project.Priorities.Add(priorities[i]);
                }
            }
            repo.Save();
            return RedirectToRoute("Project", new {username="", identifier = project.UrlName, action = "Admin"});
        }

        [Authorize]
        [HttpPost]
        public ActionResult StatusResolution(string username, string identifier, ProjectAdmin model) {
            var statuses = model.Project.Statuses;
            var resolutions = model.Project.Resolutions;
            var repo = new ProjectRepository(this.DBContext());
            var project = repo.Get(String.Concat(username, "/", identifier));
            for (int i = 0; i < statuses.Count; i++) {
                if (i < project.Statuses.Count) {
                    var db = project.Statuses[i];
                    var web = statuses[i];
                    db.Name = web.Name;
                    db.IsClosed = web.IsClosed;
                    db.IsResolved = web.IsResolved;
                } else if (!string.IsNullOrEmpty(statuses[i].Name)) {
                    statuses[i].Project = project;
                    project.Statuses.Add(statuses[i]);
                }
            }

            for (int i = 0; i < resolutions.Count; i++) {
                if (i < project.Resolutions.Count) {
                    project.Resolutions[i].Name = resolutions[i].Name;
                } else if (!string.IsNullOrEmpty(resolutions[i].Name)) {
                    resolutions[i].Project = project;
                    project.Resolutions.Add(resolutions[i]);
                }
            }

            repo.Save();
            return RedirectToRoute("Project", new {username="", identifier = project.UrlName, action = "Admin"});
        }

        [Authorize]
        [HttpPost]
        public ActionResult InviteUsers(string username, string identifier, string emailList) {
            var repo = new ProjectRepository(this.DBContext());
            var project = repo.Get(String.Concat(username, "/", identifier));
            return RedirectToRoute("Project", new {username="", identifier = project.UrlName, action = "Admin"});
        }
    }
}

