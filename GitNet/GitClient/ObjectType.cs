using System;

namespace GitNet.GitClient
{
    public enum ObjectType {
        BadObject,
        Blob,
        Commit,
        ExtendedType,
        OffsetDelta,
        ReferenceDelta,
        Tag,
        Tree,
        Reserved
    }
}

