using System;

namespace GitNet.GitClient
{
    public class PathCommitInfo
    {
        public string ShortMessage { get; set; }

        public string LongMessage { get; set; }

        public string Author { get; set; }

        public string Committer { get; set; }

        public string CommitId { get; set; }

        public string ShortId { get; set; }

        public DateTime When { get; set; }
    }
}

