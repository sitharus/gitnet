using System;
using System.IO;
using System.Security.AccessControl;
using NGit;
using NGit.Api;

namespace GitNet.GitClient
{
	public static class RepositoryAutomation
	{
        /// <summary>
        /// Creates an empty bare git repository on disk.
        /// </summary>
        /// <param name='repositoryname'>
        /// The full name of the repository to create in URL form (user/repo)
        /// </param>
		public static void CreateRepository(string repositoryname)
		{
			var path = RepositoryHelper.RepoPath (repositoryname);
			if (!Directory.Exists (path)) {
				Directory.CreateDirectory (path);
			}

			Git.Init().SetBare(true).SetDirectory(new Sharpen.FilePath(path)).Call();
		}

        /// <summary>
        /// Deletes the repository from disk, simply a recursive folder delete.
        /// </summary>
        /// <param name='repositoryname'>
        /// The full name of the repository to create in URL form (user/repo).
        /// </param>
		public static void DeleteRepository(string repositoryname)
		{
			var path = RepositoryHelper.RepoPath (repositoryname);
			if (Directory.Exists (path)) {
				Directory.Delete(path, true);
			}
		}
	}
}

