using System;
using System.IO;
using NGit;

namespace GitNet.GitClient
{
    public class RepositoryHelper
    {

        private static string RepoRoot = System.Configuration.ConfigurationManager.AppSettings.Get("RepositoryRoot");

        /// <summary>
        /// Sanitises a two-part repository path, as obtained from an MVC route in the style {username}/{reponame}
        /// </summary>
        /// <returns>
        /// The path sanitised for filesystem usage.
        /// </returns>
        /// <param name='username'>
        /// The username portion of the URL (before the /).
        /// </param>
        /// <param name='repositoryname'>
        /// The reponame portion of the URL (after the /)
        /// </param>
        public static string RepoPath(string username, string repositoryname)
        {
            var repoFileName = String.Concat(username, "-", repositoryname);
            var invalidChars = Path.GetInvalidFileNameChars();
            foreach (var c in invalidChars) {
                repoFileName = repoFileName.Replace(c, '_');
            }
            return Path.Combine(RepoRoot, repoFileName);
        }

        /// <summary>
        /// Santitises a URL-based repository path, we're expecting username/reponame here.
        /// </summary>
        /// <returns>
        /// The path sanitised for filesystem usage.
        /// </returns>
        /// <param name='repositoryname'>
        /// The repository name in URL form.
        /// </param>
        public static string RepoPath(string repositoryname)
        {
            var repoFileName = repositoryname.Replace("/", "-");
            var invalidChars = Path.GetInvalidFileNameChars();
            foreach (var c in invalidChars) {
                repoFileName = repoFileName.Replace(c, '_');
            }
            return Path.Combine(RepoRoot, repoFileName);
        }

        public static Repository GetRepository(string username, string repositoryname)
        {
            return new RepositoryBuilder().SetGitDir(new Sharpen.FilePath(RepoPath(username, repositoryname))).SetBare().ReadEnvironment().Build();
        }
    }
}

