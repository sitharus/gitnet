using System;

namespace GitNet.GitClient
{
    public class FileInfo
    {
        public string Name { get; set; }

        public bool IsSubtree { get; set; }

        public string CommitId { get; set; }

        public string FullPath { get; set; }
    }
}

