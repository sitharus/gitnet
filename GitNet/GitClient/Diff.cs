using System;
using System.Linq;
using System.Collections.Generic;
using NGit.Diff;
using NGit.Patch;

namespace GitNet.GitClient
{
    public class FileDiff {
        public string NewFilePath { get; set; }
        public string OldFilePath { get; set; }
        public bool Binary { get; set; }
        public bool Submodule { get; set; }
        public List<Hunk> Changes { get; set; }
    }

    public enum HunkType {
        Empty,
        Replace,
        Insert,
        Delete
    }

    public class Hunk {
        public int OldStart { get; set; }
        public int OldEnd {get; set;}
        public int NewStart { get; set; }
        public int NewEnd { get; set; }
        public List<string> Old { get; set; }
        public List<string> New { get; set; }
        public HunkType Type { get; set; }

        public Hunk() {
            Old = new List<string>();
            New = new List<string>();
        }

        public Hunk(Edit edit, RawText oldText, RawText newText) : this()
        {
            var type = edit.GetType();
            OldStart = edit.GetBeginA();
            OldEnd = edit.GetEndA();
            NewStart = edit.GetBeginB();
            NewEnd = edit.GetEndB();

            if (type == Edit.Type.INSERT || type == Edit.Type.REPLACE) {
                foreach (int lineNum in Enumerable.Range(NewStart, NewEnd - NewStart)) {
                    New.Add(newText.GetString(lineNum));
                }
            }

            if (type == Edit.Type.DELETE || type == Edit.Type.REPLACE) {
                foreach (int lineNum in Enumerable.Range(OldStart, OldEnd - OldStart)) {
                    Old.Add(oldText.GetString(lineNum));
                }
            }

            if (type == Edit.Type.REPLACE) {
                Type = HunkType.Replace;
            } else if (type == Edit.Type.INSERT) {
                Type = HunkType.Insert;
            } else if (type == Edit.Type.DELETE) {
                Type = HunkType.Delete;
            }
        }
    }
}

