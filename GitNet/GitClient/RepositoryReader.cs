using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using NGit;
using NGit.Api;
using NGit.Treewalk;
using NGit.Treewalk.Filter;
using NGit.Revwalk;
using NGit.Diff;
using NGit.Patch;

namespace GitNet.GitClient
{


    public class RepositoryReader
    {
        /// <summary>
        /// Lists the accessible references.
        /// </summary>
        /// <returns>
        /// The references.
        /// </returns>
        /// <param name='repo'>
        /// The repository to read from
        /// </param>
        public static List<string>ListRefs(Repository repo)
        {
            var refNames = repo.GetAllRefs();
            return refNames.Keys.ToList();
        }

        /// <summary>
        /// Lists the given tree, will error if path doesn't point to a tree.
        /// </summary>
        /// <returns>
        /// The tree.
        /// </returns>
        /// <param name='repo'>
        /// Repository to read.
        /// </param>
        /// <param name='commit'>
        /// Anything that git can resolve as a commit
        /// </param>
        /// <param name='path'>
        /// The path, null for top level.
        /// </param>
        public static List<FileInfo> ListTree(Repository repo, string commit, string path)
        {
            var items = new List<FileInfo>();

            ObjectId startTree = repo.Resolve(commit);
            if (startTree != null) {
                var resolvedCommit = ObjectId.ToString(startTree);
                var tree = new RevWalk(repo).ParseTree(startTree);
                TreeWalk walk;
                if (!string.IsNullOrEmpty(path)) {
                    walk = TreeWalk.ForPath(repo, path, tree);
                    if (walk.IsSubtree) {
                        walk.EnterSubtree();
                    }
                }
                else
                {
                    walk = new TreeWalk(repo);
                    walk.AddTree(tree);
                }
                while (walk.Next()) {
                    var info = new FileInfo { Name = walk.NameString, IsSubtree = walk.IsSubtree, CommitId = resolvedCommit, FullPath = walk.PathString };
                    items.Add(info);
                }

                walk.Release();
            }
            return items;
        }


        /// <summary>
        /// Determines whether the path is a subtree in the specified repository commit.
        /// </summary>
        /// <returns>
        /// <c>true</c> if this path is a subtree in the specified repository commit; otherwise, <c>false</c>.
        /// </returns>
        /// <param name='repo'>
        /// The repository to read
        /// </param>
        /// <param name='commit'>
        /// Anything that git can resolve as a commit
        /// </param>
        /// <param name='path'>
        /// The path, cannot be null as null is always a subtree.
        /// </param>
        public static bool IsSubtree(Repository repo, string commit, string path)
        {
            if (string.IsNullOrEmpty(path)) // root must be a tree.
            {
                return true;
            }
            ObjectId start = repo.Resolve(commit);
            if (start != null) {
                var walk = TreeWalk.ForPath(repo, path, new RevWalk(repo).ParseTree(start));
                if (walk.IsSubtree) {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Gets the BLOB specified by path.
        /// </summary>
        /// <returns>
        /// The BLOB, or null if the path is really a subtree. You should use IsSubtree() first
        /// </returns>
        /// <param name='repo'>
        /// The repository to read
        /// </param>
        /// <param name='commit'>
        /// Anything that git can resolve as a commit
        /// </param>
        /// <param name='path'>
        /// The path, cannot be null as null is always a subtree.
        /// </param>
        public static string GetBlob(Repository repo, string commit, string path)
        {
            ObjectId start = repo.Resolve(commit);
            if (start != null) {
                var revwalk = new RevWalk(repo);
                var tree = revwalk.ParseTree(start);
                var walk = TreeWalk.ForPath(repo, path, tree);
                if (walk.IsSubtree) {
                    return null;
                }
                else {
                    var id = walk.GetObjectId(0);
                    if (repo.HasObject(id)) {
                        var loader = repo.Open(id);
                        var bytes = loader.GetBytes();
                        return System.Text.Encoding.UTF8.GetString(bytes);
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Walks the history of the the specified repo, optionally limiting to path and starting from commitId.
        /// </summary>
        /// <param name='repo'>
        /// The repository to read
        /// </param>
        /// <param name='path'>
        /// The path in the repository to list, or null to list all commits
        /// </param>
        /// <param name='commitId'>
        /// The commit ref to start from, or null to start from HEAD.
        /// </param>
        public static List<PathCommitInfo> History(Repository repo, string path = null, string commitId = null)
        {
            try
            {
                var history = new Git(repo).Log();
                if (!String.IsNullOrEmpty(path)) {
                    history.AddPath(path);
                }
                if (commitId != null) {
                    history.Add(repo.Resolve(commitId));
                }
                var results = history.Call();

                return results.Select(commit =>
                                    new PathCommitInfo {
                                        ShortMessage = commit.GetShortMessage(),
                                        LongMessage = commit.GetFullMessage(),
                                        Author = commit.GetAuthorIdent().GetName(),
                                        Committer = commit.GetCommitterIdent().GetName(),
                                        When = ConvertUnixEpochTime(commit.CommitTime),
                                        CommitId = ObjectId.ToString(commit.Id),
                                        ShortId = commit.Abbreviate(6).Name
                                    }).ToList();
            } catch (NGit.Api.Errors.NoHeadException) {
                return new List<PathCommitInfo>();
            }
        }

        public static ObjectType ObjectKind(Repository repo, string objectId)
        {
            var id = ObjectId.FromString(objectId);
            if (repo.HasObject(id)) {
                var obj = repo.Open(id);

                switch (obj.GetType()) {
                case Constants.OBJ_BAD:
                    return ObjectType.BadObject;
                case Constants.OBJ_BLOB:
                    return ObjectType.Blob;
                case Constants.OBJ_COMMIT:
                    return ObjectType.Commit;
                case Constants.OBJ_EXT:
                    return ObjectType.ExtendedType;
                case Constants.OBJ_OFS_DELTA:
                    return ObjectType.OffsetDelta;
                case Constants.OBJ_REF_DELTA:
                    return ObjectType.ReferenceDelta;
                case Constants.OBJ_TAG:
                    return ObjectType.Tag;
                case Constants.OBJ_TREE:
                    return ObjectType.Tree;
                case Constants.OBJ_TYPE_5:
                    return ObjectType.Reserved;
                }
            }

            return ObjectType.BadObject;
        }

        public static List<FileDiff> CommitDiff(Repository repo, string commitId) {
            var head = repo.Resolve(commitId);
            var revWalk = new RevWalk(repo);
            var targetCommit = revWalk.ParseCommit(head);

            // Locate the trees represented by the commits
            var newTree = targetCommit.Tree;

            var parent = targetCommit.GetParent(0);

            var oldRevWalk = new RevWalk(repo);
            var previousCommit = revWalk.ParseCommit(parent);
            var oldTree = previousCommit.Tree;

            // Create the diff formatter
            var output = new System.IO.MemoryStream();
            var diffFormatter = new DiffFormatter(output);
            diffFormatter.SetRepository(repo);
            var diffEntries = diffFormatter.Scan(oldTree, newTree);

            var response = new List<FileDiff>();
            foreach (var diff in diffEntries)
            {
                var header = diffFormatter.ToFileHeader(diff);
                var fileDiff = new FileDiff {
                    NewFilePath = header.GetPath(DiffEntry.Side.NEW),
                    OldFilePath = header.GetPath(DiffEntry.Side.OLD),
                    Changes = new List<Hunk>()
                };

                if (diff.GetOldMode() == FileMode.GITLINK || diff.GetNewMode() == FileMode.GITLINK)
                {
                    fileDiff.Submodule = true;
                }
                else
                {
                    byte[] oldObj = new byte[0];
                    byte[] newObj = new byte[0];
                    var validModes = new[] {FileMode.REGULAR_FILE, FileMode.EXECUTABLE_FILE};

                    if (validModes.Contains(header.GetMode(DiffEntry.Side.OLD)))
                        oldObj = repo.Open(diff.GetOldId().ToObjectId()).GetBytes();

                    if (validModes.Contains(header.GetMode(DiffEntry.Side.NEW)))
                        newObj = repo.Open(diff.GetNewId().ToObjectId()).GetBytes();

                    if (RawText.IsBinary(oldObj) || RawText.IsBinary(newObj))
                    {
                        fileDiff.Binary = true;
                    }
                    else
                    {
                        var oldRaw = new RawText(oldObj);
                        var newRaw = new RawText(newObj);

                        foreach (var hunk in header.GetHunks())
                        {
                            foreach (var edit in hunk.ToEditList())
                            {
                                fileDiff.Changes.Add(new Hunk(edit, oldRaw, newRaw));
                            }

                        }
                    }
                }


                response.Add(fileDiff);
            }

            revWalk.Release();
            oldRevWalk.Release();
            return response;
        }

        /// <summary>
        /// Converts the given unix epoch time to a .NET DAteTime.
        /// </summary>
        /// <returns>
        /// A DateTime representing the supplied epoch time
        /// </returns>
        /// <param name='seconds'>
        /// The unix epoch time. Watch out for 2038.
        /// </param>
        private static DateTime ConvertUnixEpochTime(long seconds)
        {
            var time = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return time.AddSeconds(seconds);
        }


    }


}


